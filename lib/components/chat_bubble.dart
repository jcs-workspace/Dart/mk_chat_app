import 'package:flutter/material.dart';
import 'package:mk_chat_app/themes/theme_provider.dart';
import 'package:provider/provider.dart';

class ChatBubble extends StatelessWidget {
  final String message;
  final bool isCurrentUser;

  const ChatBubble({
    super.key,
    required this.message,
    required this.isCurrentUser,
  });

  @override
  Widget build(BuildContext context) {
    // light vs dark mode for correct bubble colors
    var themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    var isDarkMode = themeProvider.isDarkMode;

    return Container(
      decoration: BoxDecoration(
        color: isCurrentUser ? 
        isDarkMode ? Colors.green : Colors.green : 
        isDarkMode ? Colors.grey.shade500 : Colors.grey.shade500,
        borderRadius: BorderRadius.circular(12)
      ),
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Text(
        message,
        style: const TextStyle(color: Colors.white),
      ),
    );
  }
}
